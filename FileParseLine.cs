﻿using System;
using System.IO;
namespace ParserFile
{
    public class FileParseLine
    {        
        private string[] _arStr;
        private string[] _arLine;
        private string numLineBet;
        private double x;        
        private double maxSum;
        private int countLineMax;
        public FileParseLine(string path)
        {            
            _arStr = File.ReadAllLines(path);
            ReplaceString(_arStr);
        }
        public double MaxSum()
        {
            maxSum = double.MinValue;
            for (int i = 0; i < _arStr.Length; i++)
            {
                var sum = new double();
                var chekBrak = new int();
                _arLine = _arStr[i].Split(';', StringSplitOptions.RemoveEmptyEntries);
                for (int j = 0; j < _arLine.Length; j++)
                {                    
                    bool wasparsed = double.TryParse(_arLine[j], out x);
                    if (wasparsed) sum += x;
                    else
                    { 
                        chekBrak++;
                        break;
                    }
                }
                if (chekBrak == 0 && maxSum < sum && sum != 0) maxSum = sum;
            }
            return maxSum;
        }
        public string LineError()
        {            
            var countLine = new int();
            for (int i = 0; i < _arStr.Length; i++)
            {
                countLine++;
                var chekBadLine = new int();
                _arLine = _arStr[i].Split(';', StringSplitOptions.RemoveEmptyEntries);
                for (int j = 0; j < _arLine.Length; j++)
                {
                    bool wasparsed = double.TryParse(_arLine[j], out x);
                    if (wasparsed) continue;
                    else chekBadLine++;
                }
                if (chekBadLine != 0) numLineBet += countLine;
                if (chekBadLine != 0 && i < _arStr.Length - 1) numLineBet += ' ';
            }
            return numLineBet;
        }
        public int NumLineMaxSum()
        {
            maxSum = double.MinValue;
            for (int i = 0; i < _arStr.Length; i++)
            {
                var sum = new double();
                var chekBrak = new int();
                _arLine = _arStr[i].Split(';');
                for (int j = 0; j < _arLine.Length; j++)
                {
                    bool wasparsed = double.TryParse(_arLine[j], out x);
                    if (wasparsed) sum += x;
                    else
                    {
                        chekBrak++;
                        break;
                    }
                }
                if (chekBrak == 0 && maxSum < sum && sum != 0)
                {
                    maxSum = sum;
                    countLineMax = i + 1;
                }
            }
            return countLineMax;
        }
        private string[] ReplaceString(string[] Str)
        {
            for (int i = 0; i < Str.Length; i++)
            {
                Str[i] = ReplaceChar(Str[i]);
            }
            return Str;
        }
        private string ReplaceChar(string str)
        {
            str = str.Replace(" ", "@");
            str = str.Replace(";", "@");
            str = str.Replace(",", ";");
            str = str.Replace(".", ",");
            return str;
        }        
    }    
}