﻿using System;
using System.Text.RegularExpressions;

namespace ParserFile
{
    class Program
    {
        static int Main(string[] args)
        {
            var answer = "";
            var x = new int();
            var consoleArgs = new int();
            bool wasparsed;
            var path = "";
            if (args.Length != 0)
                wasparsed = int.TryParse(args[0], out consoleArgs);
            if (consoleArgs < 1 || consoleArgs > 2)
            {
                Message("--ERROR. Nids 1 or 2", TypeMessage.Error);
            }
            if (args.Length == 0)
            {
                do
                {
                    Message("1. Enter patrh file", TypeMessage.Standart);
                    Message("2. Use def file", TypeMessage.Standart);
                    answer = Answer();
                    wasparsed = int.TryParse(answer, out x);
                    if (!wasparsed)
                    {
                        Message("--ERROR. Nids 1 or 2", TypeMessage.Error);
                        Message("Tipe Enter from try ageyn", TypeMessage.Standart);
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else if (x < 1 || x > 2)
                    {
                        Message("--ERROR. Nids 1 or 2", TypeMessage.Error);
                        Message("Tipe Enter from try ageyn", TypeMessage.Standart);
                        Console.ReadLine();
                        Console.Clear();
                    }
                    Console.Clear();
                } while (x < 1 || x > 2);
            } 
            if (x == 1)
            {
                var counter = new int();
                do
                {
                    Console.Write("Enter Path:");
                    path = Answer();
                    try
                    {
                        var a = new FileParseLine(path);
                        Console.WriteLine($"Max sum: {a.MaxSum()}");
                        Console.WriteLine($"Line Error: {a.LineError()}");
                        Console.WriteLine($"Line Max Value: {a.NumLineMaxSum()}");
                        counter++;
                    }
                    catch (Exception)
                    {
                        Message("--Error, Path not found", TypeMessage.Error);
                        Message("Tipe Enter from continue", TypeMessage.Standart);
                        Answer();
                        Console.Clear();
                        continue;
                    }
                } while (counter == 0);                        
            }
            else if (x == 2 || consoleArgs == 1)
            {
                try
                {
                    var b = new FileParseLine(@"D:\line.txt");
                    Console.WriteLine($"Max sum: {b.MaxSum()}");
                    Console.WriteLine($"Line Error: {b.LineError()}");
                    Console.WriteLine($"Line Max Value: {b.NumLineMaxSum()}");
                }
                catch (Exception)
                {
                    Message("--Error, Path not found", TypeMessage.Error);
                } 
            }
            else if (consoleArgs == 2)
            {
                try
                {
                    path = args[1];
                    var c = new FileParseLine(path);
                    Console.WriteLine($"Max sum: {c.MaxSum()}");
                    Console.WriteLine($"Line Error: {c.LineError()}");
                    Console.WriteLine($"Line Max Value: {c.NumLineMaxSum()}");
                }
                catch (Exception)
                {
                    Message("--Error, Path not found", TypeMessage.Error);
                }
            }
            return 0;
        }
        public static string Answer()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            var answer = Console.ReadLine();
            Console.ResetColor();
            return answer;
        }

        public static void Message(string qwestionMassage, TypeMessage type)
        {
            if (type == TypeMessage.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (type == TypeMessage.Right)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.White;
            }
            Console.WriteLine($"{qwestionMassage}");
            Console.ResetColor();
        }
        internal enum TypeMessage
        {
            Error,
            Standart,
            Right
        }
        /*public static bool StringIsValid(string str)
        {
            bool isNull = string.IsNullOrEmpty(str);
            bool space = Regex.IsMatch(str, @"\s");
            bool abc = Regex.IsMatch(str, @"[a-zA-Z_]\w*");
            bool dubleDot = Regex.IsMatch(str, @"\.{2,}");
            bool suf = Regex.IsMatch(str, @"\W*-$");           
            if (isNull || space || abc || dubleDot || suf) return false;
            else return true;
        }*/
    }
}
